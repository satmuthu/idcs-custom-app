import React, { Component } from "react";
import { DropdownToggle, Nav, NavItem } from "reactstrap";
import PropTypes from "prop-types";
import Config from "../../config/default.json";
import { AppHeaderDropdown, AppSidebarToggler } from "@coreui/react";
//import logo from "../../assets/img/brand/logo.svg";
//import sygnet from "../../assets/img/brand/sygnet.svg";
//const backIm = require("../../assets/img/avatars/9.jpg");
const propTypes = {
  children: PropTypes.node
};
/*const sty = {
  color: "red",
  backgroundImage: "url(" + backIm + ")"
};*/
const cssOverride = {
  background: "#62a0e8",
  textTransform: "uppercase",
  fontFamily: "Lucida Console",
  alignItems: "center",
  color: "#fff",
  borderRadius: "50%",
  width: "48px",
  height: "48px",
  display: "inline-flex",
  fontSize: "20px",
  fontWeight: "bold",
  padding: "10px",
  verticalAlign: "middle",
  margin: "0 10px 0 0",
  boxShadow: "1px 0px 5px 3px rgba(0, 0, 0, 0.3)"
};
const defaultProps = {};

class DefaultHeader extends Component {
  constructor(props) {
    super(props);
    this.getUserProfile = this.getUserProfile.bind(this);
  }
  state = {
    userInitial: ""
  };
  componentDidMount() {
    let token = localStorage.getItem("token");
    if (!token) {
      setTimeout(() => {
        this.getUserProfile();
      }, 4000);
    } else {
      this.getUserProfile();
    }
  }

  getUserProfile() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "GET",
        url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      };

      request(options, function(error, response, body) {
        if (error) {
          reject(error);
        }
        if (response.statusCode === 200 && body !== undefined) {
          let data = JSON.parse(body);
          let firstName = data.name.givenName;
          let familyName = data.name.familyName;
          let initial = "";
          let ffC = firstName.substring(0, 1);
          let lfc = familyName.substring(0, 1);
          if (ffC && lfc) {
            initial = ffC + lfc;
          } else {
            initial = familyName.substring(0, 2);
          }
          currentComponent.setState({
            userInitial: initial.toLocaleUpperCase()
          });
          resolve(data);
        }
      });
    });
  }

  render() {
    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <Nav className="d-md-down-none" navbar />
        <Nav className="ml-auto" navbar>
          <NavItem className="d-md-down-none" />
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <div style={cssOverride} className="wizard-card">
                <span>{this.state.userInitial}</span>
              </div>
            </DropdownToggle>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
