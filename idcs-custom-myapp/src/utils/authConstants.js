export const IDENTITY_CONFIG = {
  authority:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com", //(string): The URL of the OIDC provider.
  client_id: "8167026aa40f482ba084f9b0180b392f", //(string): Your client application's identifier as registered with the OIDC provider.
  redirect_uri: "http://localhost:3000/signin-oidc", //The URI of your client application to receive a response from the OIDC provider.
  login: "http://localhost:3011/login",
  automaticSilentRenew: false, //(boolean, default: false): Flag to indicate if there should be an automatic attempt to renew the access token prior to its expiration.
  loadUserInfo: false, //(boolean, default: true): Flag to control if additional identity data is loaded from the user info endpoint in order to populate the user's profile.
  silent_redirect_uri: "http://localhost:3000/signin-oidc", //(string): The URL for the page containing the code handling the silent renew.
  post_logout_redirect_uri:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/userlogout", // (string): The OIDC post-logout redirect URI.
  audience: "https://sfgov.org", //is there a way to specific the audience when making the jwt
  response_type: "id_token", //(string, default: 'id_token'): The type of response desired from the OIDC provider.
  grantType: "password",
  scope: "openid", //(string, default: 'openid'): The scope being requested from the OIDC provider.
  webAuthResponseType: "access_token"
};

export const METADATA_OIDC = {
  issuer: "https://identity.oraclecloud.com/",
  jwks_uri:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/admin/v1/SigningCert/jwk",
  authorization_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/authorize",
  token_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/token",
  userinfo_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/userinfo",
  end_session_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/endsession",
  check_session_iframe:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/checksession",
  revocation_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/revocation",
  introspection_endpoint:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/introspect"
};
