import React, { Component } from "react";
import { Card, CardBody, Col, Button, Row } from "reactstrap";
import Config from "../../config/default.json";
class Logout extends Component {
  constructor(props) {
    super(props);
    this.handleLogout = this.handleLogout.bind(this);
  }
  handleLogout() {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  }
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xl="4">
            <Card className="mb-0">
              <CardBody>
                <p>Are you sure you want logout?</p>
                <Button
                  type="button"
                  size="lg"
                  name="resetButton"
                  color="primary"
                  className="btn"
                  onClick={this.handleLogout}
                >
                  <i className="fa fa-sign-out" /> Logout
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Logout;
