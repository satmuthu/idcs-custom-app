import React, { Component } from "react";
import axios from "axios";
import Config from "../../config/default.json";
import { HashLoader } from "react-spinners";
import {
  Button,
  Card,
  CardBody,
  Col,
  Form,
  Input,
  CardHeader,
  FormGroup,
  Row
} from "reactstrap";
class ChangePassword extends Component {
  state = {
    signoutTime: 1000 * 60 * 30,
    displayMessagetype: "",
    displayMessage: "",
    loading: true,
    account: {
      oldPassword: "",
      newPassword: "",
      confirmPassword: "",
      chk1: false,
      chk2: false,
      chk3: false,
      chk4: false,
      chk5: false,
      chk6: false,
      chk7: false,
      passMatch: false
    },
    name: {
      honorificPrefix: "",
      honorificSuffix: "",
      givenName: "",
      middleName: "",
      formatted: "",
      familyName: ""
    },
    userName: "",
    title: ""
  };

  updatePassword(userName, pass, newPass) {
    let currentComponent = this;
    var request = require("request");

    var options = {
      method: "POST",
      url: Config.CUST_SERVICE_ENDPOINT + "/changePassword",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      },
      body: {
        userid: userName,
        password: pass,
        newPassword: newPass
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        currentComponent.setState({
          displayMessage: "Update failed, Please try again later.",
          displayMessagetype: "Failed"
        });
      }
      if (response && body) {
        if (body === "success") {
          currentComponent.setState({
            displayMessagetype: "success",
            displayMessage: "Updated successfully",
            loading: false
          });
        } else {
          currentComponent.setState({
            displayMessage: "Update failed, Please try again later.",
            displayMessagetype: "Failed",
            loading: false
          });
        }
      }
      let account = {
        oldPassword: "",
        newPassword: "",
        confirmPassword: "",
        chk1: false,
        chk2: false,
        chk3: false,
        chk4: false,
        chk5: false,
        chk6: false,
        chk7: false,
        passMatch: false
      };
      currentComponent.setState({
        account: account
      });
      setTimeout(() => {
        currentComponent.setState({
          displayMessagetype: "",
          displayMessage: ""
        });
      }, 5000);
    });
  }

  componentDidMount() {
    let currentComponent = this;
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    };
    axios
      .get(Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me", { headers: headers })
      .then(function(response) {
        const data = response.data;
        const name = data.name;
        const userName = data.userName;
        currentComponent.setState({ name: name, userName: userName });
        currentComponent.setState({
          loading: false
        });
      })
      .catch(error => {
        if (error && error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
      });

    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];

    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }

    this.setTimeout();
  }

  handleSubmit = e => {
    e.preventDefault();

    let oldPass = this.state.account.oldPassword;
    let newPass = this.state.account.newPassword;
    let conPass = this.state.account.confirmPassword;

    if (!oldPass && !newPass && !conPass) {
      return;
    }

    this.setState({
      loading: true
    });
    this.updatePassword(this.state.userName, oldPass, newPass);
  };

  handleOnChange = e => {
    const account = this.state.account;
    account[e.currentTarget.name] = e.currentTarget.value;
    const pass = account.newPassword;
    this.validatePassword(pass);
    let validpass =
      account.chk1 &&
      account.chk2 &&
      account.chk3 &&
      account.chk4 &&
      account.chk5 &&
      account.chk6 &&
      account.chk7;
    let pmatch =
      account.newPassword !== "" &&
      account.newPassword === account.confirmPassword;
    validpass && pmatch && account.oldPassword !== ""
      ? (account.passMatch = true)
      : (account.passMatch = false);
    this.setState({ account });
  };

  validatePassword(val) {
    let firstName =
      this.state.name.givenName !== undefined
        ? this.state.name.givenName.toLowerCase()
        : "";
    let lastName =
      this.state.name.familyName !== undefined
        ? this.state.name.familyName.toLowerCase()
        : "";
    const data = this.state.account;
    if (!val && val.length === 0) {
      data.chk1 = false;
      data.chk2 = false;
      data.chk3 = false;
      data.chk4 = false;
      data.chk5 = false;
      data.chk6 = false;
      data.chk7 = false;
      return;
    }
    let lowercasePass = val.toLowerCase();
    const splch = "[!@#//[\\]$%^&*+~(){},.:;_=<>?/|-]+";
    if (val.length >= 10) {
      data.chk1 = true;
    } else {
      data.chk1 = false;
    }
    if (val.trim().match(/[A-Z]/g)) {
      data.chk2 = true;
    } else {
      data.chk2 = false;
    }
    if (val.trim().match(/[0-9]/g)) {
      data.chk3 = true;
    } else {
      data.chk3 = false;
    }
    if (val.trim().match(splch) != null) {
      data.chk4 = true;
    } else {
      data.chk4 = false;
    }
    if (val.trim().match(/[a-z]/g)) {
      data.chk5 = true;
    } else {
      data.chk5 = false;
    }
    if (firstName !== "") {
      if (!lowercasePass.includes(firstName)) {
        data.chk6 = true;
      } else {
        data.chk6 = false;
      }
    } else {
      data.chk6 = true;
    }
    if (lastName !== "") {
      if (!lowercasePass.includes(lastName)) {
        data.chk7 = true;
      } else {
        data.chk7 = false;
      }
    } else {
      data.chk7 = true;
    }

    this.setState({ data });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="sweet-loading spinner">
          <HashLoader
            sizeUnit={"px"}
            size={50}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>
        <Row>
          <Col xl="10">
            <strong>
              <span
                className={
                  this.state.displayMessagetype === "success"
                    ? "color-success"
                    : "color-error"
                }
              >
                {this.state.displayMessage}
              </span>
            </strong>
          </Col>
        </Row>
        <Row>
          <Col xl="10">
            <Card className="reset-form">
              <CardHeader>
                <strong>Set a new password for your user account</strong>
              </CardHeader>
              <CardBody>
                <Form onSubmit={this.handleSubmit} className="form-horizontal">
                  <FormGroup row>Your Password must have at least:</FormGroup>
                  <FormGroup row>
                    <ul className="rest-form-li">
                      <li>
                        <i
                          className={
                            this.state.account.chk1 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; 10 characters
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk2 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; One uppercase character
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk3 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; One numeric character
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk4 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; One special character
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk5 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; One lowercase character
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk6 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; Password should not contain your first name
                      </li>
                      <li>
                        <i
                          className={
                            this.state.account.chk7 === true
                              ? "fa fa-check color-success"
                              : "fa fa-times-circle color-error"
                          }
                        />
                        &nbsp; Password should not contain your last name
                      </li>
                    </ul>
                  </FormGroup>
                  <FormGroup row>
                    <Input
                      type="password"
                      onChange={this.handleOnChange}
                      autoFocus
                      bsSize="lg"
                      id="oldPassword"
                      name="oldPassword"
                      className="input-normal"
                      placeholder="Old Password"
                      value={this.state.account.oldPassword}
                    />
                  </FormGroup>
                  <FormGroup row>
                    <Input
                      type="password"
                      onChange={this.handleOnChange}
                      bsSize="lg"
                      id="newPassword"
                      name="newPassword"
                      className="input-normal"
                      placeholder="New Password"
                      value={this.state.account.newPassword}
                    />
                  </FormGroup>
                  <FormGroup row>
                    <Input
                      type="password"
                      onChange={this.handleOnChange}
                      bsSize="lg"
                      id="confirmPassword"
                      name="confirmPassword"
                      className="input-normal"
                      placeholder="Confirm Password"
                      value={this.state.account.confirmPassword}
                    />
                  </FormGroup>
                  <Button
                    type="submit"
                    size="lg"
                    name="resetButton"
                    color="primary"
                    className="btn"
                    disabled={!this.state.account.passMatch}
                    onClick={this.handleSubmit}
                  >
                    <i className="fa fa-dot-circle-o" /> Submit
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }

  clearTimeoutFunc = () => {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  };

  setTimeout = () => {
    this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
  };

  resetTimeout = () => {
    this.clearTimeoutFunc();
    this.setTimeout();
  };

  logout = () => {
    console.log("Sending a logout request...");
    this.destroy();
  };

  destroy = () => {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  };
}

export default ChangePassword;
