import React, { Component } from "react";
import axios from "axios";
import Config from "../../config/default.json";
import { HashLoader } from "react-spinners";

import {
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Card,
  CardHeader,
  CardBody
} from "reactstrap";

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.onChange = this.onChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }
  state = {
    disabledInput: true,
    signoutTime: 1000 * 60 * 30,
    loading: true,
    displayMessage: "",
    displayMessagetype: "",
    homePhone: {
      verified: "",
      type: "",
      primary: "",
      value: "",
      display: ""
    },
    mobilePhone: {
      verified: "",
      type: "",
      primary: "",
      value: "",
      display: ""
    },
    workPhone: {
      verified: "",
      type: "",
      primary: "",
      value: "",
      display: ""
    },
    primaryMail: {
      secondary: "",
      verified: "",
      primary: "",
      value: "",
      type: ""
    },
    secondaryMail: {
      secondary: "",
      verified: "",
      primary: "",
      value: "",
      type: ""
    },
    workAddress: {
      locality: "",
      region: "",
      streetAddress: "",
      country: "",
      type: "",
      postalCode: ""
    },
    name: {
      honorificPrefix: "",
      honorificSuffix: "",
      givenName: "",
      middleName: "",
      formatted: "",
      familyName: ""
    },
    userName: "",
    title: ""
  };

  componentDidMount() {
    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];
    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }
    this.setTimeout();

    let currentComponent = this;
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    };
    axios
      .get(Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me", { headers: headers })
      .then(function(response) {
        const data = response.data;
        const phones = data.phoneNumbers;
        const emails = data.emails;
        const name = data.name;

        if (phones !== undefined) {
          const workPhone = phones.find(function(phone) {
            return phone.type === "work";
          });
          if (workPhone) {
            currentComponent.setState({ workPhone });
          }
          const mobilePhone = phones.find(function(phone) {
            return phone.type === "mobile";
          });
          if (mobilePhone) {
            currentComponent.setState({ mobilePhone });
          }
          const homePhone = phones.find(function(phone) {
            return phone.type === "home";
          });
          if (homePhone) {
            currentComponent.setState({ homePhone });
          }
        }

        const primaryMail = emails.find(function(email) {
          return email.primary === true;
        });

        currentComponent.setState({ user: data });
        currentComponent.setState({ primaryMail: primaryMail });
        currentComponent.setState({ name: name });
        currentComponent.setState({ userName: data.userName });
        currentComponent.setState({ title: data.title });
        currentComponent.setState({ loading: false });
      })
      .catch(error => {
        if (error && error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
      });
  }
  onChange(e) {
    if (e.currentTarget.name === "homePhone") {
      let homePhone = this.state.homePhone;
      homePhone.value = e.currentTarget.value;
      this.setState({ homePhone: homePhone });
    }
    if (e.currentTarget.name === "mobilePhone") {
      let mobilePhone = this.state.mobilePhone;
      mobilePhone.value = e.currentTarget.value;
      this.setState({ mobilePhone: mobilePhone });
    }
    if (e.currentTarget.name === "workPhone") {
      let workPhone = this.state.workPhone;
      workPhone.value = e.currentTarget.value;
      this.setState({ workPhone: workPhone });
    }
    if (e.currentTarget.name === "title") {
      this.setState({ title: e.currentTarget.value });
    }
    if (e.currentTarget.name === "honorificPrefix") {
      let name = this.state.name;
      name.honorificPrefix = e.currentTarget.value;
      this.setState({ name: name });
    }
    if (e.currentTarget.name === "honorificSuffix") {
      let name = this.state.name;
      name.honorificSuffix = e.currentTarget.value;
      this.setState({ name: name });
    }
    if (e.currentTarget.name === "givenName") {
      let name = this.state.name;
      name.givenName = e.currentTarget.value;
      this.setState({ name: name });
    }
    if (e.currentTarget.name === "middleName") {
      let name = this.state.name;
      name.middleName = e.currentTarget.value;
      this.setState({ name: name });
    }
    if (e.currentTarget.name === "formatted") {
      let name = this.state.name;
      name.formatted = e.currentTarget.value;
      this.setState({ name: name });
    }
    if (e.currentTarget.name === "familyName") {
      let name = this.state.name;
      name.familyName = e.currentTarget.value;
      this.setState({ name: name });
    }
  }
  handleFormSubmit() {
    if (!this.state.name.familyName || !this.state.name.givenName) {
      this.setState({
        displayMessagetype: "failure",
        displayMessage: "Please enter all required fields."
      });
      setTimeout(() => {
        this.setState({
          displayMessagetype: "",
          displayMessage: ""
        });
      }, 5000);
      return false;
    }
    let currentComponent = this;
    let user = {
      title: "",
      userName: "",
      emails: [],
      phoneNumbers: [],
      name: {
        honorificPrefix: "",
        honorificSuffix: "",
        givenName: "",
        middleName: "",
        formatted: "",
        familyName: ""
      },
      schemas: ["urn:ietf:params:scim:schemas:core:2.0:User"]
    };
    user.emails.push(this.state.primaryMail);
    if (this.state.workPhone.value !== "") {
      let workPhone = {
        type: "work",
        value: this.state.workPhone.value
      };
      user.phoneNumbers.push(workPhone);
    }
    if (this.state.homePhone.value !== "") {
      let homePhone = {
        type: "home",
        value: this.state.homePhone.value
      };
      user.phoneNumbers.push(homePhone);
    }
    if (this.state.mobilePhone.value !== "") {
      let mobilePhone = {
        type: "mobile",
        value: this.state.mobilePhone.value
      };
      user.phoneNumbers.push(mobilePhone);
    }
    if (this.state.title) {
      user.title = this.state.title;
    } else {
      delete user.title;
    }
    if (this.state.name.honorificPrefix) {
      user.name.honorificPrefix = this.state.name.honorificPrefix;
    } else {
      // delete user.name.honorificPrefix;
    }
    if (this.state.name.honorificSuffix) {
      user.name.honorificSuffix = this.state.name.honorificSuffix;
    } else {
      delete user.name.honorificSuffix;
    }
    if (this.state.name.givenName) {
      user.name.givenName = this.state.name.givenName;
    } else {
      delete user.name.givenName;
    }
    if (this.state.name.middleName) {
      user.name.middleName = this.state.name.middleName;
    } else {
      delete user.name.middleName;
    }
    if (this.state.name.formatted) {
      user.name.formatted = this.state.name.formatted;
    } else {
      delete user.name.formatted;
    }
    if (this.state.name.familyName) {
      user.name.familyName = this.state.name.familyName;
    } else {
      delete user.name.familyName;
    }

    // user.name = this.state.name;
    user.userName = this.state.userName;
    fetch(Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me", {
      method: "PUT",
      body: JSON.stringify(user),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    }).then(response => {
      if (response.status === 200) {
        currentComponent.setState({
          displayMessagetype: "success",
          displayMessage: "Updated successfully"
        });
      } else {
        currentComponent.setState({
          displayMessagetype: "failure",
          displayMessage: "Updated failed. Please try again later"
        });
      }
      if (response && response.statusCode === 401) {
        localStorage.clear();
        window.location.replace(Config.LOGOUT_URL);
      }
      setTimeout(() => {
        currentComponent.setState({
          displayMessagetype: "",
          displayMessage: ""
        });
      }, 5000);
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <div className="sweet-loading spinner">
            <HashLoader
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
        </div>
      );
    } else {
      return (
        <Row>
          <Col xl="10">
            <Card className="mb-0">
              <CardHeader id="headingTwo">
                <h5 className="m-0 p-0">My Profile</h5>
              </CardHeader>
              <CardBody>
                <div className="animated fadeIna">
                  <Row>
                    <Col>
                      <Form action="" method="GET">
                        <FormGroup row>
                          <Col className="pull-left">
                            <strong>
                              <span
                                className={
                                  this.state.displayMessagetype === "success"
                                    ? "color-success"
                                    : "color-error"
                                }
                              >
                                {this.state.displayMessage}
                              </span>
                            </strong>
                          </Col>
                          {/* <Col className="pull-right">
                            <Button
                              type="button"
                              size="lg"
                              name="resetButton"
                              color="primary"
                              className="btn"
                              onClick={this.handleFormSubmit}
                            >
                              <i className="fa fa-dot-circle-o" /> Submit
                            </Button>
                              </Col>*/}
                        </FormGroup>
                        <FormGroup row>
                          <Col md="5">
                            <div className="divpro">
                              <Label htmlFor="userName">User Name</Label>
                              <Input
                                type="text"
                                name="userName"
                                disabled={this.state.disabledInput}
                                id="userName"
                                value={this.state.userName}
                                onChange={this.onChange}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="honorificPrefix">Prefix</Label>
                              <Input
                                name="honorificPrefix"
                                id="honorificPrefix"
                                type="text"
                                disabled={this.state.disabledInput}
                                value={this.state.name.honorificPrefix}
                                onChange={this.onChange}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="givenName"> * First Name</Label>
                              <Input
                                name="givenName"
                                id="givenName"
                                type="text"
                                disabled={this.state.disabledInput}
                                value={this.state.name.givenName}
                                onChange={this.onChange}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="middleName">Middle Name</Label>
                              <Input
                                name="middleName"
                                id="middleName"
                                type="text"
                                disabled={this.state.disabledInput}
                                value={this.state.name.middleName}
                                onChange={this.onChange}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="familyName">* Last Name</Label>
                              <Input
                                name="familyName"
                                id="familyName"
                                type="text"
                                disabled={this.state.disabledInput}
                                value={this.state.name.familyName}
                                onChange={this.onChange}
                              />
                            </div>
                          </Col>
                          <Col md="5">
                            <div className="divpro">
                              <Label htmlFor="primaryMail"> * Email</Label>
                              <Input
                                name="primaryMail"
                                id="primaryMail"
                                type="text"
                                disabled={this.state.disabledInput}
                                value={this.state.primaryMail.value}
                                onChange={this.onChange}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="homePhone">
                                Home Phone Number
                              </Label>
                              <Input
                                name="homePhone"
                                id="homePhone"
                                disabled={this.state.disabledInput}
                                onKeyDown={e =>
                                  (e.keyCode === 69 || e.keyCode === 190) &&
                                  e.preventDefault()
                                }
                                type="number"
                                onChange={this.onChange}
                                value={this.state.homePhone.value}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="mobilePhone">
                                Mobile Phone Number
                              </Label>
                              <Input
                                name="mobilePhone"
                                id="mobilePhone"
                                type="number"
                                disabled={this.state.disabledInput}
                                onKeyDown={e =>
                                  (e.keyCode === 69 || e.keyCode === 190) &&
                                  e.preventDefault()
                                }
                                onChange={this.onChange}
                                value={this.state.mobilePhone.value}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="workPhone">
                                Work Phone Number
                              </Label>
                              <Input
                                name="workPhone"
                                id="workPhone"
                                disabled={this.state.disabledInput}
                                type="number"
                                onKeyDown={e =>
                                  (e.keyCode === 69 || e.keyCode === 190) &&
                                  e.preventDefault()
                                }
                                onChange={this.onChange}
                                value={this.state.workPhone.value}
                              />
                            </div>
                            <div className="divpro">
                              <Label htmlFor="honorificSuffix">Suffix</Label>
                              <Input
                                name="honorificSuffix"
                                id="honorificSuffix"
                                type="text"
                                disabled={this.state.disabledInput}
                                onChange={this.onChange}
                                value={this.state.name.honorificSuffix}
                              />
                            </div>
                          </Col>
                        </FormGroup>
                        <FormGroup row>
                          <Col md="5">
                            <div className="divpro">
                              <Label htmlFor="title">Title</Label>
                              <Input
                                name="title"
                                disabled={this.state.disabledInput}
                                id="title"
                                type="text"
                                onChange={this.onChange}
                                value={this.state.title}
                              />
                            </div>
                          </Col>
                        </FormGroup>
                      </Form>
                    </Col>
                  </Row>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      );
    }
  }
  clearTimeoutFunc = () => {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  };

  setTimeout = () => {
    this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
  };

  resetTimeout = () => {
    this.clearTimeoutFunc();
    this.setTimeout();
  };

  logout = () => {
    console.log("Sending a logout request...");
    this.destroy();
  };

  destroy = () => {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  };
}

export default MyProfile;
