var OidcSettings = {
  authority:
    "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com/oauth2/v1/token",
  client_id: "b33733d64f4742f287b5e6d269f23bcf",
  redirect_uri: "https://localhost:3000/#/dashboard",
  response_type: "code",
  scope: "openid,profile,email",
  post_logout_redirect_uri: "https://localhost:3011/"
};
