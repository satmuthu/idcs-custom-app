export default {
  items: [
    {
      name: "Dashboard",
      url: "/dashboard",
      icon: "icon-speedometer"
    },
    {
      name: "Change Password",
      url: "/changePassword",
      icon: "icon-pencil"
    },
    {
      name: "Email Options",
      url: "/emailOptions",
      icon: "icon-bell"
    },
    {
      name: "Multi Factor",
      url: "/multiFactor",
      icon: "icon-puzzle"
    },
    {
      name: "My Profile",
      url: "/profile",
      icon: "icon-note"
    },
    {
      name: "Logout",
      url: "/logout",
      icon: "icon-logout"
    }
  ]
};
