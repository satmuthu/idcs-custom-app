var User = (function() {
  var full_name = "";
  var first_name = "";
  var last_name = "";
  var token = "";
  var getFullName = function() {
    return full_name;
  };

  var setFullName = function(name) {
    full_name = name;
  };
  var getFirstName = function() {
    console.log("============In the Head" + first_name);
    return first_name;
  };

  var setFirstName = function(name) {
    first_name = name;
  };
  var getLastName = function() {
    return last_name;
  };

  var setLastName = function(name) {
    last_name = name;
  };
  var getToken = function() {
    return token;
  };

  var setToken = function(token) {
    token = token;
  };
  return {
    getFullName: getFullName,
    setFullName: setFullName,
    setFirstName: setFirstName,
    getFirstName: getFirstName,
    getLastName: getLastName,
    setLastName: setLastName,
    getToken: getToken,
    setToken: setToken
  };
})();

export default User;
