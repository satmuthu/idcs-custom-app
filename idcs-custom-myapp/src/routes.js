import React from "react";

const Dashboard = React.lazy(() => import("./views/Dashboard"));
const ChangePassword = React.lazy(() => import("./views/ChangePassword"));
const EmailOptions = React.lazy(() => import("./views/EmailOptions"));
const MyProfile = React.lazy(() => import("./views/MyProfile"));
const MultiFactor = React.lazy(() => import("./views/MultiFactor"));
const Logout = React.lazy(() => import("./views/Logout"));

const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/changePassword",
    name: "Change Password",
    component: ChangePassword
  },
  {
    path: "/emailOptions",
    name: "Email Options",
    component: EmailOptions
  },
  {
    path: "/multiFactor",
    name: "Multi Factor",
    component: MultiFactor
  },
  {
    path: "/profile",
    name: "My Profile",
    component: MyProfile
  },
  {
    path: "/logout",
    name: "Logout",
    component: Logout
  }
];

export default routes;
