var ids = {
  oracle: {
    ClientId: "b33733d64f4742f287b5e6d269f23bcf",
    ClientSecret: "d3c86f8a-99de-44a9-b69b-db53cd74da88",
    ClientTenant: "idcs-28d2cb948ab8484aaeb29f302ed52c7d",
    IDCSHost:
      "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com",
    AudienceServiceUrl:
      "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com",
    TokenIssuer: "https://identity.oraclecloud.com/",
    scope: "urn:opc:idm:t.user.me openid",
    logoutSufix: "/oauth2/v1/userlogout",
    redirectURL: "http://localhost:3000/#/dashboard",
    LogLevel: "trace",
    ConsoleLog: "True"
  }
};

module.exports = ids;

///TO_DO Set all the url's here
