var request = require("request");
var jwt = require("jsonwebtoken");
var logger = require("./logging");
var idcsCrypto = require("./idcsCrypto.js");
var auth = require("../auth.js");
var config = require("../config.js");
var defaultConfig = require("../defaultConfig.json");

// urn:opc:idm:__myscopes__ will get all of the IDCS scopes granted to the app
// I don't want that.
//
// the three scopes we do want are:
// urn:opc:idm:t.user.signin          - to perform the login API calls
// urn:opc:idm:t.user.mecreate        - to perform self registration
// urn:opc:idm:t.user.resetpassword   - to make password reset request calls
//
// if you disable any of those functions in the app you can
// (and probably should) remove the associated scope from here
let neededScopes = [
  "urn:opc:idm:t.security.client",
  "urn:opc:idm:t.user.signin",
  "urn:opc:idm:t.user.mecreate",
  "urn:opc:idm:t.user.forgotpassword",
  "urn:opc:idm:t.user.resetpassword",
  "urn:opc:idm:t.user.verifyemail"
];

// those scopes are currently included in these IDCS app roles:
var necessaryAppRoles = [
  "Authenticated Client",
  "Forgot Password",
  "Reset Password",
  "Self Registration",
  "Signin",
  "Verify Email"
];
// the 'Authenticated Client' one comes for free so you only need to grant your app the other 3

// compares 2 sorted arrays to make sure the contents are the same
// this isn't a complete function, it's just as much as I need
function isEqual(a1, a2) {
  if (a1.length != a2.length) {
    return false;
  }
  for (var i = 0; i < a1.length; i++) {
    if (a1[i] != a2[i]) return false;
  }
  return true;
}

// the getAT function goes and gets an AT from IDCS
function getAT() {
  return new Promise(function(resolve, reject) {
    request(
      {
        method: "POST",
        uri: auth.oracle.IDCSHost + "/oauth2/v1/token",
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Authorization:
            "Basic " +
            Buffer.from(
              auth.oracle.ClientId + ":" + auth.oracle.ClientSecret
            ).toString("base64"),
          Accept: "application/json"
        },

        body:
          "grant_type=client_credentials&scope=" +
          encodeURIComponent(neededScopes.join(" "))
      },
      function(error, response, body) {
        if (error) logger.error("error: " + error);
        if (response && response.statusCode) {
          logger.log("statusCode: " + response.statusCode);
        }
        logger.log("body: " + body);

        if (response && 200 == response.statusCode) {
          var bodydata = JSON.parse(body);
          let token = bodydata.access_token;
          var decoded = jwt.decode(token);

          // NOTE: I am **INTENTIONALLY** doing this every time
          //       I could do this check once in the startup but I want these
          //       warnings to appear over and over and over so they can't be missed!
          //
          // Special check: make sure the clientAppRoles claim has exactly what we expect
          //
          // Missing roles are non fatal as long as we have the necessary scopes
          var clientAppRoles = decoded.clientAppRoles.sort(); // sort them to make the compare easier

          if (!isEqual(clientAppRoles, necessaryAppRoles)) {
            logger.error("");
            logger.error("");
            logger.error("");
            logger.error("!!!!!! WARNING !!!!!!");
            logger.error("!!!!!! WARNING !!!!!!");
            logger.error("!!!!!! WARNING !!!!!!");
            logger.error("");
            logger.error("");
            logger.error("Application configured incorrectly!");
            logger.error(
              "Sign in application should have **ONLY** these " +
                necessaryAppRoles.length +
                " IDCS app roles granted:"
            );
            necessaryAppRoles.forEach(function(role) {
              logger.error(' * "' + role + '"');
            });
            logger.error("");
            logger.error("Your application has the following instead:");
            clientAppRoles.forEach(function(role) {
              logger.error(' * "' + role + '"');
            });
            logger.error("");
            logger.error(
              "The acquired token will only contain absolutely necessary scopes,"
            );
            logger.error(
              "but this is a **POTENTIAL SECURITY ISSUE** in your configuration"
            );
            logger.error("and should be remedied ASAP!");
            logger.error("");
            logger.error("");
          }

          // NOTE:
          //       If the config is wrong when we spin up throwing here will force
          //       the server to exit. This is intentional!
          //
          //       We throw rather than exiting to deal with the case where we
          //       initialize properly. But someone later changes IDCS' config
          //       (while we're running).
          //       In that case the throw() will result in a 500 error appearing
          //       in the end user's browser, but the server won't shutdown.
          //       As soon as the admin corrects the mistake the app will begin
          //       working again without needing to be restarted.

          // OAuth allows a client to request more scopes than it is allowed to have.
          // The only way to know if we got everything we need is to actually check.
          // This is where I do that check
          let missingScopes = [];
          neededScopes.forEach(function(scope) {
            if (decoded.scope.indexOf(scope) == -1) missingScopes.push(scope);
          });
          if (missingScopes.length > 0) {
            logger.error(
              "ERROR: Token does not have required scopes " +
                missingScopes.join(", ")
            );
            // don't tell the user which is missing. Just that there's a config error.
            // the admin will have to look at the logs to know what they did wrong.
            throw "Unable to continue due to configuration error";
          }

          // if we got here then things are OK
          resolve(token);
        } else {
          throw "Failed to acquire Access Token. Check client ID, Secret, and IDCS URL.";
          // and also for sunspots, or leopards in the server room?
        }
      }
    );
  });
}

exports.getAT = getAT;

const initialized = false;

// We need the signing cert to verify the signature on POST data
// We get the cert from the JWKS URL. But to talk to that we need an AT
// So we wire this function in to be called as soon as we acquire the AT.
//
// In the future we may be able to just use the Client ID and secret.
// Enh 27896624
function getSigningKeyOriginal(accessToken) {
  return new Promise(function(resolve, reject) {
    request(
      {
        method: "GET",
        uri: auth.oracle.IDCSHost + "/admin/v1/SigningCert/jwk",
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + accessToken,
          Accept: "application/json"
        }
      },
      function(error, response, body) {
        if (error) logger.log("error: " + error);
        if (response && response.statusCode) {
          logger.log("statusCode: " + response.statusCode);
        }
        logger.log("body: " + body);

        if (response && 200 == response.statusCode) {
          var bodydata = JSON.parse(body);
          logger.log(JSON.stringify(bodydata, null, 2));
          // we need the first (and probably only) cert from there
          if (bodydata.keys && bodydata.keys[0] && bodydata.keys[0].x5c) {
            logger.log("Extracting x5c from first JWKS key");

            var x5c = bodydata.keys[0].x5c[0];

            // PEM format says that lines must be no more than 64 chars
            // so rewrap the x5c content 64 bytes at a pop
            var cert = "-----BEGIN CERTIFICATE-----\n";
            while (x5c.length > 0) {
              if (x5c.length > 64) {
                cert += x5c.substring(0, 64) + "\n";
                x5c = x5c.substring(64, x5c.length);
              } else {
                cert += x5c;
                x5c = "";
              }
            }
            cert += "\n-----END CERTIFICATE-----\n";

            logger.log("Cert: \n" + cert);
            idcsCrypto.setTenantCert(cert);
            return;
          }
        }

        // if we get down to here there was a problem.
        // for now we just throw a generic error.
        // since this function is only called during startup throwing here
        // will crash out of the startup and shut the server down.
        // I *think* that's what we want.
        throw "Failed to acquire certificate from JWKS URI!";
      }
    );
  });
}

function getSigningKey(accessToken) {
  return new Promise(function(resolve, reject) {
    request(
      {
        method: "GET",
        uri: auth.oracle.IDCSHost + "/admin/v1/SigningCert/jwk",
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + accessToken,
          Accept: "application/json"
        }
      },
      function(error, response, body) {
        if (error) {
          return reject(error);
        } else if (response && response.statusCode == 200) {
          var bodydata = JSON.parse(body);

          if (bodydata.keys && bodydata.keys[0] && bodydata.keys[0].x5c) {
            var x5c = bodydata.keys[0].x5c[0];
            var cert = "-----BEGIN CERTIFICATE-----\n";
            while (x5c.length > 0) {
              if (x5c.length > 64) {
                cert += x5c.substring(0, 64) + "\n";
                x5c = x5c.substring(64, x5c.length);
              } else {
                cert += x5c;
                x5c = "";
              }
            }
            cert += "\n-----END CERTIFICATE-----\n";
            idcsCrypto.setTenantCert(cert);
            logger.log("Cert: \n" + cert);
            return resolve(cert);
          }
        } else {
          return reject("Unable to get certificate from JWKS URI.");
        }
      }
    );
  });
}

function authorize(value) {
  return new Promise(function(resolve, reject) {
    logger.log("--- Authorizing request...");
    if (!value) {
      return reject("Invalid token.");
    }
    let valueArray = value.split(" ");
    let tokenType = valueArray[0];
    let tokenValue = valueArray[1];
    if (tokenType && tokenType.toLowerCase() !== "bearer") {
      return reject("Invalid token.");
    }
    let cert = idcsCrypto.getTenantCert();
    logger.log("--- Cert: \n" + cert);
    logger.log("--- Signing cert obtained. Verifying JWT...");
    jwt.verify(
      tokenValue,
      cert,
      {
        sub: auth.oracle.ClientId,
        issuer: "https://identity.oraclecloud.com/",
        ignoreExpiration: "true"
      },
      function(error, decoded) {
        if (error) {
          logger.log("--- Verification failed: " + error.message);
          return reject("Invalid token.");
        }
        if (decoded) {
          logger.log(
            "--- Verification ok. Token decoded: " + JSON.stringify(decoded)
          );
          return resolve(decoded);
        }
      }
    );
  });
}

exports.authorize = authorize;

//-------------------UI Client API------------------------//

async function verifyMyPassord(user, pass, token) {
  return new Promise(function(resolve, reject) {
    const requestBody = JSON.stringify({
      mappingAttribute: "userName",
      mappingAttributeValue: user,
      password: pass,
      schemas: [
        "urn:ietf:params:scim:schemas:oracle:idcs:PasswordAuthenticator"
      ]
    });

    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + token
    };
    request(
      {
        method: "POST",
        uri: auth.oracle.IDCSHost + "/admin/v1/PasswordAuthenticator",
        headers: headers,
        body: requestBody
      },
      async function(error, response, body) {
        if (error) {
          logger.log("Password validation failed" + error);
          reject(error);
        }
        if (response && 201 === response.statusCode) {
          resolve("SUCCESS");
        }
        if (response && 201 !== response.statusCode) {
          resolve("FAILED");
        }
      }
    );
  });
}
exports.verifyMyPassord = verifyMyPassord;

async function getAdminToken() {
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "POST",
      uri: auth.oracle.IDCSHost + "/oauth2/v1/token",
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(
            auth.oracle.ClientId + ":" + auth.oracle.ClientSecret
          ).toString("base64")
      },
      form: {
        grant_type: "client_credentials",
        scope: "urn:opc:idm:__myscopes__"
      }
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.log("Get Admin Token failed" + error);
        reject("FAILED");
      }
      if (response && 200 === response.statusCode) {
        let bodyData = JSON.parse(body);
        resolve(bodyData.access_token);
      } else if (response && 201 !== response.statusCode) {
        resolve("FAILED");
      }
    });
  });
}
exports.getAdminToken = getAdminToken;

function sendPasswordRecoveryOTP(userName, otpType, otpValue) {
  if (userName === null || otpType === null || otpValue === null) {
    logger.log("SendPasswordrecoveryOTP : Invalid fields.");
    return "Invalid fields.";
  }
  return new Promise(function(resolve, reject) {
    var request = require("request");
    var bodyData = {};
    if (otpType === "EMAIL") {
      bodyData = {
        ForgotPassword: {
          UserLogin: userName,
          OTPChannel: "EMAIL",
          UserEmailId: otpValue
        }
      };
    } else if (otpType === "SMS") {
      bodyData = {
        ForgotPassword: {
          UserLogin: userName,
          OTPChannel: "SMS",
          UserMobileNumber: otpValue
        }
      };
    }
    var options = {
      method: "POST",
      url: defaultConfig.PASSWORD_RECOVERY_URL + "/sendOTP",
      headers: {
        "Content-Type": "application/json"
      },
      body: bodyData,
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.error("Request to get OTP token failed.");
        reject("FAILED");
      }
      if (response && 200 === response.statusCode && body) {
        let bodyData = JSON.parse(body);
        resolve(bodyData.MessageStatus);
      }
    });
  });
}
exports.sendPasswordRecoveryOTP = sendPasswordRecoveryOTP;

function verifyPassordRecoveryOTP(userName, otpValue) {
  if (userName === null || otpValue === null) {
    return "Invalid fields.";
  }
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "POST",
      url: defaultConfig.PASSWORD_RECOVERY_URL + "/verifyOTP",
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        ForgotPassword: {
          UserLogin: userName,
          UserGivenOTP: otpValue
        }
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.error("Request to verify OTP access token failed.");
        reject("FAILED");
      }
      if (response && 200 === response.statusCode && body) {
        let bodyData = JSON.parse(body);
        resolve(bodyData.MessageStatus);
      } else {
        resolve(response);
      }
    });
  });
}
exports.verifyPassordRecoveryOTP = verifyPassordRecoveryOTP;

function otpResetPassord(userName, password) {
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "POST",
      url: defaultConfig.PASSWORD_RECOVERY_URL + "/resetPwd",
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        ForgotPassword: {
          UserLogin: userName,
          NewPassword: password
        }
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.error("Request to verify OTP access token failed.");
        reject("FAILED");
      }
      if (response && 200 === response.statusCode && body) {
        let bodyData = JSON.parse(body);
        resolve(bodyData.MessageStatus);
      }
      if (response && 400 === response.statusCode && body) {
        let bodyData = JSON.parse(body);
        resolve(bodyData);
      }
    });
  });
}
exports.otpResetPassord = otpResetPassord;

async function getAccessToken(token) {
  return new Promise(function(resolve, reject) {
    var options = {
      method: "POST",
      uri: auth.oracle.IDCSHost + "/oauth2/v1/token",
      headers: {
        Authorization:
          "Basic " +
          Buffer.from(
            auth.oracle.ClientId + ":" + auth.oracle.ClientSecret
          ).toString("base64"),
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      form: {
        grant_type: "urn:ietf:params:oauth:grant-type:jwt-bearer",
        scope: "urn:opc:idm:__myscopes__",
        assertion: token
      }
    };

    request(options, async function(error, response, body) {
      if (error) {
        logger.error("Request to get access token failed. Invalid id token");
        reject(error);
      }
      if (response && 200 === response.statusCode) {
        var bodydata = JSON.parse(body);
        resolve(bodydata.access_token);
      } else if (response && response.statusCode !== 200) {
        logger.error("Request to get access token failed.");
        resolve("FAILED");
      }
    });
  });
}
exports.getAccessToken = getAccessToken;

function getPasswordRecoveryMethods() {}

function getUserRecoveryEmail(token) {
  var request = require("request");

  var options = {
    method: "GET",
    uri: auth.oracle.IDCSHost + "/admin/v1/Me",
    headers: {
      Authorization: "Bearer " + token
    }
  };

  request(options, function(error, response, body) {
    if (error) {
      logger.error("Request to get access token failed. Invalid id token");
      reject(error);
    }
    if (response && 200 === response.statusCode) {
      var bodydata = JSON.parse(body);

      let emails = bodydata.emails;
      const secondaryMail = emails.find(function(email) {
        return email.type === "recovery";
      });
      let usr = {
        id: bodydata.id,
        userName: bodydata.userName,
        recoveryEmail: secondaryMail.value
      };
      resolve(usr);
    } else if (response && response.statusCode !== 200) {
      logger.error("Request to get access token failed.");
      resolve("FAILED");
    }
  });
}

function getAllMFAOptions(token) {
  let currentComponent = this;
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "GET",
      url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/MyDevices",
      headers: {
        "accept-encoding": "gzip, deflate",
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      }
    };

    request(options, async function(error, response, body) {
      if (error) {
        if (error.response.status === 401) {
          window.location.replace(Config.LOGIN_URL);
        } else {
          reject(error);
        }
      }
      if (response.statusCode === 200 && body !== undefined) {
        currentComponent.setState({
          loading: false,
          listEnrolledQRDevices: [],
          listEnrolledMobileNumbers: []
        });
        let data = JSON.parse(body);
        let list = data.Resources;
        let mobileList = currentComponent.state.listEnrolledMobileNumbers;
        let QRdiviceList = currentComponent.state.listEnrolledQRDevices;
        list.map(function(item) {
          let divice = {
            lastSyncTime: item.lastSyncTime,
            id: item.id,
            phoneNumber: item.phoneNumber,
            status: item.status,
            displayName: item.displayName,
            authenticationFactors: item.authenticationFactors
          };
          if (
            item.authenticationFactors[0] !== undefined &&
            item.authenticationFactors[0].type === "EMAIL" &&
            item.authenticationFactors[0].status === "ENROLLED"
          ) {
            currentComponent.setState({
              userHasMFASetup: true,
              emailSetupProcessCompleted: true,
              otpEnrolledEmail: divice
            });
          } else if (
            item.authenticationFactors[0] !== undefined &&
            item.authenticationFactors[0].type === "SMS" &&
            item.authenticationFactors[0].status === "ENROLLED"
          ) {
            mobileList.push(divice);
          } else if (
            item.authenticationFactors[0] !== undefined &&
            item.authenticationFactors[0].status === "ENROLLED" &&
            (item.authenticationFactors[0].type === "PUSH" ||
              item.authenticationFactors[0].type === "TOTP")
          ) {
            QRdiviceList.push(divice);
          }
        });

        currentComponent.setState({
          loading: false,
          listEnrolledQRDevices: QRdiviceList,
          listEnrolledMobileNumbers: mobileList
        });
        if (mobileList.length > 0) {
          currentComponent.setState({
            userHasMFASetup: true,
            hideEnroleMobileSMSOTPMessage: true
          });
        } else {
          currentComponent.setState({
            hideEnroleMobileSMSOTPMessage: false
          });
        }
        if (QRdiviceList.length > 0) {
          currentComponent.setState({
            userHasMFASetup: true,
            hideEnroleMobileSMSOTPMessage: true
          });
        } else {
          currentComponent.setState({
            hideEnroleMobileSMSOTPMessage: false
          });
        }
        currentComponent.setState({
          displayRemoveOTPDeviceModal: false
        });
        resolve(response);
      }
    });
  });
}

//-------------------UI Client API------------------------//

// only do this once:
if (!initialized) {
  // go get an initial AT not only to check the config, but
  // also to set the tenant ID.
  // We will need that later to decode the post data.
  logger.log("Acquiring initial Access Token...");
  getAT()
    .then(function(accessToken) {
      logger.log("Acquired initial Access Token successfully:");
      logger.log(accessToken + "\n");

      // then acquire the tenant signing certificate
      // we should only need to do this once
      // TODO: think about if we need to do this more often
      //getSigningKey(accessToken)
      getSigningKey(accessToken).then(function() {
        logger.log("Looking for tenant name in Access Token...\n");
        var decoded = jwt.decode(accessToken);
        if (decoded["user.tenant.name"]) {
          let tenantName = decoded["user.tenant.name"];
          logger.log("Tenant name is: " + tenantName);
          idcsCrypto.setTenantName(tenantName);
        }
      });
    })
    .catch(err => logger.log(err));
}
